[require]
GLSL >= 4.30

[vertex shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_VERTEX_SHADER
#define IN_OUT out
#define MAX_PASS
#define COPY_DEPTH

#ifndef USE_GPU_SHADER_CREATE_INFO
out vec4 uvcoordsvar;
#endif

void main()
{
  int v = gl_VertexID % 3;
  float x = -1.0 + float((v & 1) << 2);
  float y = -1.0 + float((v & 2) << 1);
  gl_Position = vec4(x, y, 1.0, 1.0);
  uvcoordsvar = vec4((gl_Position.xy + 1.0) * 0.5, 0.0, 0.0);
}

[fragment shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_FRAGMENT_SHADER
#define IN_OUT in
#define MAX_PASS
#define COPY_DEPTH
/**
 * Shader that down-sample depth buffer,
 * saving min and max value of each texel in the above mipmaps.
 * Adapted from http://rastergrid.com/blog/2010/10/hierarchical-z-map-based-occlusion-culling/
 *
 * Major simplification has been made since we pad the buffer to always be bigger than input to
 * avoid mipmapping misalignment.
 */

#ifdef LAYERED
uniform sampler2DArray depthBuffer;
uniform int depthLayer;
#else
uniform sampler2D depthBuffer;
#endif

#ifndef COPY_DEPTH
uniform vec2 texelSize;
#endif

#ifdef LAYERED
#  define sampleLowerMip(t) texture(depthBuffer, vec3(t, depthLayer)).r
#  define gatherLowerMip(t) textureGather(depthBuffer, vec3(t, depthLayer))
#else
#  define sampleLowerMip(t) texture(depthBuffer, t).r
#  define gatherLowerMip(t) textureGather(depthBuffer, t)
#endif

#ifdef MIN_PASS
#  define minmax2(a, b) min(a, b)
#  define minmax3(a, b, c) min(min(a, b), c)
#  define minmax4(a, b, c, d) min(min(min(a, b), c), d)
#else /* MAX_PASS */
#  define minmax2(a, b) max(a, b)
#  define minmax3(a, b, c) max(max(a, b), c)
#  define minmax4(a, b, c, d) max(max(max(a, b), c), d)
#endif

/* On some AMD card / driver combination, it is needed otherwise,
 * the shader does not write anything. */
#if (defined(GPU_INTEL) || defined(GPU_ATI)) && defined(GPU_OPENGL)
out vec4 fragColor;
#endif

void main()
{
  vec2 texel = gl_FragCoord.xy;

#ifdef COPY_DEPTH
  vec2 uv = texel / vec2(textureSize(depthBuffer, 0).xy);

  float val = sampleLowerMip(uv);
#else
  /* NOTE(@fclem): textureSize() does not work the same on all implementations
   * when changing the min and max texture levels. Use uniform instead (see T87801). */
  vec2 uv = texel * 2.0 * texelSize;

  vec4 samp;
#  ifdef GPU_ARB_texture_gather
  samp = gatherLowerMip(uv);
#  else
  samp.x = sampleLowerMip(uv + vec2(-0.5, -0.5) * texelSize);
  samp.y = sampleLowerMip(uv + vec2(-0.5, 0.5) * texelSize);
  samp.z = sampleLowerMip(uv + vec2(0.5, -0.5) * texelSize);
  samp.w = sampleLowerMip(uv + vec2(0.5, 0.5) * texelSize);
#  endif

  float val = minmax4(samp.x, samp.y, samp.z, samp.w);
#endif

#if (defined(GPU_INTEL) || defined(GPU_ATI)) && defined(GPU_OPENGL)
  /* Use color format instead of 24bit depth texture */
  fragColor = vec4(val);
#endif

#if !(defined(GPU_INTEL) && defined(GPU_OPENGL))
  /* If using Intel workaround, do not write out depth as there will be no depth target and this is
   * invalid. */
  gl_FragDepth = val;
#endif
}

